# Details

## ingest.php

The ingest.php script is called when the Apache2 server recieves a requests. The script will respond with an error "invalid request, try again" if any type of request other than a POST is recieved. Upon recieving a POST, the script will take the request's contents and convert them into a valid postback object for the Redis server. Then, the script will connect to the Redis server and perform a Right Push onto a list.

The format of the postback object is a single string with the following format: [Method] [URL with data injected].

Upon completing the Right Push onto the Redis server, we send the user back "successful POST".

## deliver.go

The deliver app is written in Golang and is used to postback requests from the queue and perform them. In addition to this upon each postback, it will log the following information: the request code, response time, and the response text. It saves this information into a log file located at /tmp/dat.log. The app is written so that upon execution, it will connect to the Redis server and go into an infinite loop where it will perform a blocking Left Pop to get the postback and then immediately send it as a GET request to its target URL. This method of using a blocking Pop which allows for the infinite loop to efficiently collect requests off of the queue with no wasted iterations of the loop.

# Install

## PHP and Apache2

Follow steps outline in this article skipping mySQL setup and PHP module setup

https://www.digitalocean.com/community/tutorials/how-to-install-linux-apache-mysql-php-lamp-stack-on-ubuntu-16-04

Modify the ingest.ini to contain updated port and password values for Redis server.

Copy ingest.php and ingest.ini into var/www/html/
```sh
sudo cp ingest.php /var/www/html/
```

## Redis

Follow quickstart guide

https://redis.io/topics/quickstart

Update configuration file to require password and use custom Port

In the Security section of redis.conf add the line "requirepass [password]"

In the Network section of redis.conf add the line "port [custom port]"

## PHPRedis

Follow this article

https://anton.logvinenko.name/en/blog/how-to-install-redis-and-redis-php-client.html

## GoLang

Follow install guide

Set Environmental variables REDIS\_PORT and REDIS\_PASS to respective values 

https://golang.org/doc/install

Be sure to run ```go test``` once.

## gosexy/redis Library

Follow installation instructions in Readme

https://menteslibres.net/gosexy/redis

# Usage

## Start up Apache Server

```sh
$ sudo service apache2 start
```

## Start up Redis Server

In installation directory src

```sh
$ ./redis-server ../redis.conf (optional --daemonize yes)
```

## Start up Delivery.go app

Run go build in go-agent dir

```sh
$ go build
```

Run app

```sh
$ ./go-agent
```

Now you are able to send POST requests to http://localhost/ingest.php with the proper endpoint and data JSON objects and it will feed them into a queue within Redis and the Golang app will read the items of the queue and perform the Postback.

## Example 1

### Sample Request:
    (POST) http://{server_ip}/ingest.php
	{  
      "endpoint":{  
        "method":"GET",
        "url":"https://httpbin.org/get?evil={evil}"
      },
      "data":[
        {
          "evil":"money"
        }
      ]
    }

### Sample Response (Postback):
    GET https://httpbin.org/get?evil=money

### Sample dat.log
```sh
{
	"Code":200,
	"Time":0.142557617,
	"Text":"{
		\n  \"args\": {
			\n    \"evil\": \"money\"\n  
		},
	 	\n  \"headers\": {
	 		\n    \"Accept-Encoding\": \"gzip\",
	 		\n    \"Connection\": \"close\", 
	 		\n    \"Host\": \"httpbin.org\", 
	 		\n    \"User-Agent\": \"Go-http-client/1.1\"\n  
	 	},
	 	\n  \"origin\": \"138.197.5.163\", 
	 	\n  \"url\": \"https://httpbin.org/get?evil=money\"\n
	 } \n"
}
```
## Example 2

### Sample Request:
    (POST) http://{server_ip}/ingest.php
	{  
      "endpoint":{  
        "method":"GET",
        "url":"https://httpbin.org/get?cat={cat}&dog={dog}"
      },
      "data":[
        {
          "cat":"Tabby",
          "dog":"Poodle"
        },
        {
          "cat":"Siamese",
          "dog":"Yellow Lab"
        }
      ]
    }

### Sample Response (Postback):
    GET https://httpbin.org/get?cat=Tabby&dog=Poodle

    GET https://httpbin.org/get?cat=Siamese&dog=Yellow%20Lab

### Sample dat.log
```sh
{
	"Code":200,
	"Time":0.113671121,
	"Text":"{
		\n  \"args\": {
			\n    \"cat\": \"Siamese\", 
			\n    \"dog\": \"Yellow Lab\"\n  
		}, 
		\n  \"headers\": {
			\n    \"Accept-Encoding\": \"gzip\", 
			\n    \"Connection\": \"close\", 
			\n    \"Host\": \"httpbin.org\", 
			\n    \"User-Agent\": \"Go-http-client/1.1\"\n  
		}, \n  \"origin\": \"138.197.5.163\", 
		\n  \"url\": \"https://httpbin.org/get?cat=Siamese\u0026dog=Yellow+Lab\"\n
	}\n"
}
```


