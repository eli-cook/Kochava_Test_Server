package deliver

import "log"
import "menteslibres.net/gosexy/redis"
import "net/http"
import "io/ioutil"
import "time"
import "os"
import "encoding/json"
import (
	"errors"
	"strconv"
)

// basic struct to hold log information
// will be immediately converted into JSON data and
// written to file
type PostbackLog struct {
	Code         int
	ResponseTime float64
	DeliveryTime string
	Text         string
	Error        string
}

type Postback struct {
	Method string `json:"method"`
	Url    string `json:"url"`
}

type BLPopper interface {
	BLPop(timeout uint64, keys ...string) ([]string, error)
}

func Run() {

	port, _ := strconv.Atoi(os.Getenv("REDIS_PORT"))

	// connects to Redis Server and authenticates
	client := redis.New()

	err := client.Connect("redis", uint(port))
	if err != nil {
		log.Fatalf("Connection failed: %s\n", err.Error())
		return
	}
	log.Println("Connected to Redis Server!")
	_, err = client.Auth(os.Getenv("REDIS_PASS"))
	if err != nil {
		log.Fatalf(err.Error())
	}

	// open/create logging file
	f, err := os.OpenFile("/tmp/data/dat.log", os.O_CREATE|os.O_APPEND|os.O_WRONLY, 0666)
	if err != nil {
		log.Fatalf("Failed to open/create log file")
	}
	defer f.Close()

	// infinite loop to handle queue of requests off of
	// Redis server
	for {
		var jsonLog []byte

		// Attempt to pull from Redis, and create error log if error exists
		// else deliver postback and create error log if it returns an error
		// otherwise it was a successful transaction, so write the log to JSON
		if postback, err := PullFromRedis(client); err != nil {
			jsonLog = ErrorToJson(err, time.Now)
		} else if postbackLog, err := DeliverPostback(postback, time.Since); err != nil {
			jsonLog = ErrorToJson(err, time.Now)
		} else {
			jsonLog = LogToJson(postbackLog)
		}

		// write generated log to file
		length, err := f.Write(jsonLog)
		if err != nil {
			log.Println(err)
			log.Println(length)
			log.Println("Failed to write JSON to file, LogToJson DATA")
		}
	}
}

// ErrorToJson converts an error to a PostbackLog then to a JSON representation of it.
// It takes in a error and returns a JSON string in a byte array.
func ErrorToJson(err error, timeNow func() time.Time) []byte {
	errorLog := PostbackLog{
		Error:        err.Error(),
		DeliveryTime: timeNow().String(),
	}

	jsonlog, _ := json.Marshal(errorLog)
	return jsonlog
}


// LogToJson converts a PostbackLog to a JSON representation of it.
// It takes in a PostbackLog and return a JSON string in a byte array.
func LogToJson(requestlog PostbackLog) []byte {
	jsonlog, _ := json.Marshal(requestlog)
	return jsonlog
}


// PullFromRedis pulls a Postback JSON string from the server and converts to a Postback object.
// It takes a interface BLPopper and returns a Postback object and an error.
func PullFromRedis(client BLPopper) (Postback, error) {

	payload, err := client.BLPop(0x0, "mylist")

	if err != nil {
		return Postback{}, err
	}

	var postback Postback
	err = json.Unmarshal([]byte(payload[1]), &postback)
	if err != nil {
		return Postback{}, err
	}

	return postback, nil
}

// DeliverPostback converts a Postback object to a PostbackLog.
// It takes a Postback object and a time function.
// It returns a PostbackLog and an error.
func DeliverPostback(postback Postback, timeSince func(time.Time) time.Duration) (PostbackLog, error) {

	var requestlog PostbackLog

	if postback.Method == "GET" {

		// calculate response time and send GET request
		start := time.Now()
		resp, err := http.Get(postback.Url)
		elapsed := timeSince(start)

		if err != nil {
			log.Println("Failed to send GET request")
			return PostbackLog{}, err
		} 
		log.Println("Successfully sent GET request")

		var body []byte
		body, err = ioutil.ReadAll(resp.Body)
		if err != nil {
			return PostbackLog{}, err
		}
		resp.Body.Close()
		requestlog.Text = string(body)

		deliv, _ := http.ParseTime(resp.Header.Get("Date"))
		requestlog.DeliveryTime = deliv.String()

		requestlog.Code = resp.StatusCode

		requestlog.ResponseTime = elapsed.Seconds()

		return requestlog, nil

	} else {
		log.Println("Unsupported Method")
		return PostbackLog{}, errors.New("Unsupported Method: " + postback.Method)
	}
}
