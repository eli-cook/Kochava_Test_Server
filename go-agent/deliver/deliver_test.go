package deliver

import "github.com/stretchr/testify/assert"
import "github.com/stretchr/testify/mock"
import "testing"
import "net/http/httptest"
import "net/http"
import "time"
import "errors"

func TestLogToJson(t *testing.T) {

	testLog := PostbackLog{
		Code:         200,
		ResponseTime: 0.123,
		DeliveryTime: "2009-11-17 20:34:58.651387237 +0000 UTC",
		Text:         "Important Data",
	}

	expectedJsonLog := []byte("{\"Code\":200,\"ResponseTime\":0.123,\"DeliveryTime\":\"2009-11-17 20:34:58.651387237 +0000 UTC\",\"Text\":\"Important Data\",\"Error\":\"\"}")

	testJson := LogToJson(testLog)

	assert.Equal(t, expectedJsonLog, testJson)
}

func TestErrorToJson(t *testing.T) {
	testError := errors.New("something failed")
	expectedJsonLog := []byte("{\"Code\":0,\"ResponseTime\":0,\"DeliveryTime\":\"2009-11-17 20:34:58.651387237 +0000 UTC\",\"Text\":\"\",\"Error\":\"something failed\"}")

	testJson := ErrorToJson(testError, myDate)

	assert.Equal(t, expectedJsonLog, testJson)
}

func TestPullFromRedisFailedPop(t *testing.T) {
	m := new(MockRedis)

	m.On("BLPop", uint64(0), []string{"mylist"}).Return([]string{"blank", "{\"method\":\"GET\",\"url\":\"http://google.com\"}"}, errors.New("BLPOP somehow errored"))
	expectedError := errors.New("BLPOP somehow errored")

	_, err := PullFromRedis(m)

	assert.Equal(t, expectedError, err)
}

func TestPullFromRedisFailedJSON(t *testing.T) {
	m := new(MockRedis)

	m.On("BLPop", uint64(0), []string{"mylist"}).Return([]string{"blank", "{\"method\":\"GET\",\"url\":\"http://google.com\""}, nil)
	expectedError := errors.New("unexpected end of JSON input")

	_, err := PullFromRedis(m)

	assert.Equal(t, expectedError.Error(), err.Error())
}

func TestPullFromRedisSuccess(t *testing.T) {
	m := new(MockRedis)

	m.On("BLPop", uint64(0), []string{"mylist"}).Return([]string{"blank", "{\"method\":\"GET\",\"url\":\"http://google.com\"}"}, nil)
	expectedPostback := Postback{
		Method: "GET",
		Url:    "http://google.com",
	}

	testPostback, _ := PullFromRedis(m)

	assert.Equal(t, expectedPostback, testPostback)
}

func TestDeliverPostbackUnsupportedMethod(t *testing.T) {

	input := Postback{
		Method: "POST",
		Url:    "https://httpbin.org/get?evil=money",
	}
	output := PostbackLog{
		Error: "Unsupported Method: POST",
	}

	assert := assert.New(t)
	testlog, err := DeliverPostback(input, myTime)

	assert.Equal(output.Code, testlog.Code)
	assert.Equal(output.ResponseTime, testlog.ResponseTime)
	assert.Equal(output.DeliveryTime, testlog.DeliveryTime)
	assert.Equal(output.Text, testlog.Text)
	assert.Equal(output.Error, err.Error())
}

func TestDeliverPostbackValidRequest(t *testing.T) {

	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Date", "0001-01-01 00:00:00 +0000 UTC")
		w.WriteHeader(http.StatusServiceUnavailable)
		w.Write([]byte("Error"))
	}))
	defer ts.Close()

	input := Postback{
		Method: "GET",
		Url:    ts.URL,
	}
	output := PostbackLog{
		Code:         503,
		Text:         "Error",
		DeliveryTime: "0001-01-01 00:00:00 +0000 UTC",
		ResponseTime: 0.123,
	}

	assert := assert.New(t)
	testlog, _ := DeliverPostback(input, myTime)

	assert.Equal(output.Code, testlog.Code)
	assert.Equal(output.ResponseTime, testlog.ResponseTime)
	assert.Equal(output.DeliveryTime, testlog.DeliveryTime)
	assert.Equal(output.Text, testlog.Text)
	assert.Equal(output.Error, testlog.Error)
}

func TestDeliverPostbackInvalidRequest(t *testing.T) {

	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Date", "0001-01-01 00:00:00 +0000 UTC")
		w.WriteHeader(http.StatusServiceUnavailable)
		w.Write([]byte("Error"))
	}))
	defer ts.Close()

	input := Postback{
		Method: "GET",
		Url:    "",
	}
	output := PostbackLog{
		Code:         0,
		Text:         "",
		ResponseTime: 0,
		Error:        "Get : unsupported protocol scheme \"\"",
	}

	assert := assert.New(t)
	testlog, err := DeliverPostback(input, myTime)

	assert.Equal(output.Code, testlog.Code)
	assert.Equal(output.ResponseTime, testlog.ResponseTime)
	assert.Equal(output.Text, testlog.Text)
	assert.Equal(output.Error, err.Error())
}

func myTime(time.Time) time.Duration {

	val, _ := time.ParseDuration("123ms")
	return val
}

func myDate() time.Time {
	return time.Date(2009, 11, 17, 20, 34, 58, 651387237, time.UTC)
}

type MockRedis struct {
	mock.Mock
}

func (m *MockRedis) BLPop(timeout uint64, keys ...string) ([]string, error) {
	args := m.Called(timeout, keys)
	return args.Get(0).([]string), args.Error(1)
}
