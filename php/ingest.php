<?php

$ini = parse_ini_file('ingest.ini');

// initial response, assume bad.
$response = "invalid data, try again";

if ($_SERVER["REQUEST_METHOD"] == "POST") {

	// get request data and convert it to associative arrays
        $postData = json_decode(file_get_contents('php://input'), true);
        $endpoint = $postData['endpoint'];
        $url = (string) $endpoint['url'];

        // Get data fields to replace from URL
        preg_match_all('/{(.*?)}/', $url, $params);
        $fields = $params[1];

        // startup connection to Redis server
        $redis = new Redis();
        $redis->connect('redis', $ini['db_port']);
        if($redis->auth($ini['db_password'])) {
        	$response = 'successfully connected to redis server...';
        }
        else {
        	$response = 'failed to connect to redis server';
                return;
        }

        // construct a postback object per data object and send it to Redis
        foreach ($postData['data'] as $data) {

                $postback = $endpoint;

                // craft new url with replaced data members
                $tempUrl = $url;
                foreach ($fields as $f) {
                     $tempUrl = str_replace('{'.$f.'}', urlencode($data[$f]), $tempUrl);   
                }
        	$postback['url'] = $tempUrl;

        	// push new postback object onto redis queue
        	if($redis->rPush('mylist', json_encode($postback,JSON_UNESCAPED_SLASHES)) == FALSE)
        	 	print_r('failed to push data to redis');
        }

        $response = $response . "successful POST";
}

?>

<html>
        <body>
                <a><?php echo $response ?></a>
        </body>
</html>